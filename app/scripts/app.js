(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'start': '/',
      'home': '/home',
      'pokemon-detail': '/pokemon'
    }
  });
}());
