import { CellsPage } from '@cells/cells-page';
import { html, css } from 'lit-element';

import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-web-panel-info/bbva-web-panel-info.js';

class StartPage extends CellsPage {
  static get is() {
    return 'start-page';
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
        </div>
        <div slot="app__main" class="container">
          <bbva-web-panel-info button-label="Let´s Start" id="panel-info" @action-click="${this._goHome}">
            <img slot="micro" src="resources/images/pokemon_home.png" alt="home">
              a simple Pokemon Guide
          </bbva-web-panel-info>
        </div>
      </cells-template-paper-drawer-panel>
    `;
  }

  onPageEnter() {
    this.shadowRoot.getElementById('panel-info').renderRoot.querySelector('.micro').style.width = 'auto';
  }

  _goHome() {
    this.publish('ch_init_pokemon_list');
    this.navigate('home');
  }

  static get styles() {
    return css`
      .container {
        background-color: aliceblue;
        height: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
      }
    `;
  }
}

window.customElements.define(StartPage.is, StartPage);

