import { CellsPage } from '@cells/cells-page';
import { html, css } from 'lit-element';

import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-header-main';
import '@bbva-web-components/bbva-button-default/bbva-button-default.js';
import '@bbva-web-components/bbva-web-panel-microillustration/bbva-web-panel-microillustration.js';
import '@bbva-web-components/bbva-progress-content/bbva-progress-content.js';
import '@capa-cells/cells-pokeapi-dm/cells-pokeapi-dm.js';

class HomePage extends CellsPage {
  static get is() {
    return 'home-page';
  }

  constructor() {
    super();
    this.pageKey = 0;
    this.pageSize = 12;
    this.previous = '<';
    this.next = '>';
  }

  static get properties() {
    return {
      pageKey: { type: Number },
      pageSize: { type: Number },
      previous: { type: String },
      next: { type: String },
      pokemonList: { type: Object },
      detail: { type: Object }
    };
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left-primary="coronita:return-12"
            accessibility-text-icon-left-primary="Volver"
            text='Home'
            @header-main-icon-left-primary-click=${() => window.history.back()}>
          </bbva-header-main>
        </div>
        <div slot="app__main" class="container">
          <bbva-progress-content show-spinner-message-label="" variant="transparent">Loading</bbva-progress-content>
          <div class="buttons">
            <bbva-web-button-default size="l" ?disabled="${this.pageKey === 0}" @click="${() => this._getList('P')}">${this.previous}</bbva-web-button-default>
            <span>${this.pageKey + 1}</span>
            <bbva-web-button-default size="l" @click="${() => this._getList('N')}">${this.next}</bbva-web-button-default>
          </div>
          <div class="list">
            ${this.pokemonList ? this._showList : ''}
          </div>
        </div>
      </cells-template-paper-drawer-panel>
      <cells-pokeapi-dm id="pokemonDm"
        @list-success="${this._pokemonList}"
        @detail-success="${this._pokemonDetail}">
      </cells-pokeapi-dm>
    `;
  }

  onPageEnter() {
    this.subscribe('ch_init_pokemon_list', this._getList());
  }

  _getList(option) {
    this.shadowRoot.querySelector('bbva-progress-content').setAttribute('visible', '');
    if (option === 'P') {
      this.pageKey--;
    } else if (option === 'N') {
      this.pageKey++;
    }
    this.shadowRoot.getElementById('pokemonDm').getPokemonList(this.pageKey, this.pageSize);
  }

  get _showList() {
    return this.pokemonList.map(pokemon => this._showPokemon(pokemon));
  }

  _showPokemon(pokemon) {
    return html`
      <bbva-web-panel-microillustration class="min" heading="${pokemon.name}" @click="${() => {
  this._getDetail(pokemon.url);
}}">
        <img slot="micro" src="${this._getImage(pokemon.url)}" alt="pokemon">
      </bbva-web-panel-microillustration>
    `;
  }

  _pokemonList(ev) {
    this.pokemonList = ev.detail.results;
    this.shadowRoot.querySelector('bbva-progress-content').removeAttribute('visible');
  }

  _getImage(url) {
    return 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + url.replace('https://pokeapi.co/api/v2/pokemon/', '').replace('/', '') + '.png';
  }

  _getDetail(url) {
    this.shadowRoot.querySelector('bbva-progress-content').setAttribute('visible', '');
    this.shadowRoot.getElementById('pokemonDm').getPokemonDetail(url);
  }

  _pokemonDetail(ev) {
    this.detail = ev.detail;
    this.shadowRoot.querySelector('bbva-progress-content').removeAttribute('visible');
    this.publish('ch_pokemon_detail', ev.detail);
    this.navigate('pokemon-detail');
  }

  static get styles() {
    return css`
      .container {
        background-color: aliceblue;
        min-height: 100%;
      }

      .list {
        display: flex;
        flex-wrap: wrap;
      }

      bbva-web-panel-microillustration {
        margin: 25px;
        border: 1px solid #ccc;
        flex: 20%;
      }

      .buttons {
        text-align: center;
        padding: 10px;
      }
    `;
  }
}

window.customElements.define(HomePage.is, HomePage);

