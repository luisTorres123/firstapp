import { CellsPage } from '@cells/cells-page';
import { html, css } from 'lit-element';

import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-header-main';
import '@bbva-web-components/bbva-carousel-swipe/bbva-carousel-swipe.js';
import '@bbva-web-components/bbva-list-simple/bbva-list-simple.js';
import '@bbva-web-components/bbva-web-divider/bbva-web-divider.js';
import '@bbva-web-components/bbva-list-progress/bbva-list-progress.js';

class PokemonDetail extends CellsPage {
  static get is() {
    return 'pokemon-detail-page';
  }

  constructor() {
    super();
  }

  static get properties() {
    return {
      detail: { type: Object },
      items: { type: Object }
    };
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left-primary="coronita:return-12"
            accessibility-text-icon-left-primary="Volver"
            text='Detail'
            @header-main-icon-left-primary-click=${() => window.history.back()}>
          </bbva-header-main>
        </div>
        <div slot="app__main" class="container">
          <div class="description">
            ${this.items ? this._getCarousel : ''}
            <div class="right-desc">
              <div class="title">
                <span>STATS</span>
              </div>
              <div class="stats">
                ${this.detail ? this._getStats : ''}
              </div>
            </div>
          </div>
          <div class="title">
            <span>MOVES</span>
          </div>
          <div class="moves"> 
            ${this.detail ? this._getMoves : ''}
          </div>
        </div>
      </cells-template-paper-drawer-panel>
    `;
  }

  onPageEnter() {
    this.subscribe('ch_pokemon_detail', (ev) => {
      this.detail = ev;
    });
    this._getCarouselItems(this.detail.sprites);
  }

  get _getCarousel() {
    return html`<bbva-carousel-swipe id="carousel" .items="${this.items}"></bbva-carousel-swipe>`;
  }

  _getCarouselItems(sprites) {
    this.items = [
      { content: html`<img src="${sprites.front_default}" alt="pokemon" style="width:100%; height: 100%;">` },
      { content: html`<img src="${sprites.back_default}" alt="pokemon" style="width:100%; height: 100%;">` },
      { content: html`<img src="${sprites.front_shiny}" alt="pokemon" style="width:100%; height: 100%;">` },
      { content: html`<img src="${sprites.back_shiny}" alt="pokemon" style="width:100%; height: 100%;">` },
    ];
  }

  get _getMoves() {
    return this.detail.moves.map(move => this._showMove(move));
  }

  _showMove(move) {
    return html`
      <bbva-list-simple
        variant="stacked"
        description-medium
        label="${move.move.name}">
      </bbva-list-simple>
      <bbva-web-divider size="fit"></bbva-web-divider>
    `;
  }

  get _getStats() {
    return this.detail.stats.map(stat => this._showStat(stat));
  }

  _showStat(stat) {
    let max = this._getMaxStatValue(stat.stat.name);
    return html`
      <bbva-list-progress max-value="${max}" current-value="${stat.base_stat}">
        <bbva-core-heading slot="title">
          <span>${stat.stat.name}</span>
          <span>${stat.base_stat}</span>
        </bbva-core-heading>
      </bbva-list-progress>
    `;
  }

  _getMaxStatValue(statName) {
    switch (statName) {
      case 'hp': return 714;
      case 'attack': return 3024;
      case 'defense': return 2760;
      case 'special-attack': return 3024;
      case 'special-defense': return 5526;
      case 'speed': return 8664;
    }
  }

  static get styles() {
    return css`
      .container {
        height: 100%;
      }
      bbva-carousel-swipe {
        width: 25%;
      }
      .description {
        display: flex;
      }
      .moves {
        height: 70%;
        overflow: scroll;
      }
      .title {
        justify-content: center;
        display: flex;
        align-items: center;
        height: 5%;
        background-color: skyblue;
      }
      .stats {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        height: 100%;
      }
      .right-desc {
        width: 75%;
      }
    `;
  }
}

window.customElements.define(PokemonDetail.is, PokemonDetail);

